
// Created by andrew on 22.05.18.

#include <stdlib.h>
#include <string.h>
#include "TreeView.h"
#include "data/Queue.h"
#include "data/Stack.h"
#include "Draw.h"
#include "Utilites.h"

void initTreeView(struct TreeObjects treeObjects){
    treeView.field = treeObjects.field;
    treeView.statusbar = treeObjects.statusbar;
    treeView.stub = treeObjects.stub;
    treeView.mes = treeObjects.mes;
    treeView.css = treeObjects.css;

    treeView.xSize = 1060;      treeView.ySize = 620;
    treeView.elWidth = 200;     treeView.elHeight = 100;
    treeView.borderTop = 20;    treeView.borderRight = 20;
    treeView.borderLeft = 20;   treeView.borderBottom = 20;
    treeView.xGap = 20;         treeView.yGap = 50;

    descr.root = NULL;
}

void addItem(struct value d){
    if (descr.root==NULL){
        gtk_container_remove(GTK_CONTAINER(treeView.field), GTK_WIDGET(treeView.stub));
        gtk_widget_set_css_provider(GTK_WIDGET(treeView.field), treeView.css);
        descr.root = (struct item*) calloc(1, sizeof(struct item));
        descr.root->dic = d;
        descr.root->right = NULL;
        descr.root->left = NULL;
        setMessage(descr.root->dic.key, 'a');
        repaint();
    }
    else
        addItemRec(descr.root, d);
}

void addItemRec(struct item *node, struct value d){
    if (strcmp(node->dic.key, d.key)==0) {
        setMessage(d.key, 'e');
        return;
    }
    else if (strcmp(node->dic.key, d.key) > 0) {
        if (node->left == NULL) {
            struct item *temp = (struct item *) calloc(1, sizeof(struct item));
            strcpy(temp->dic.key, d.key);
            strcpy(temp->dic.val, d.val);
            temp->left = NULL;
            temp->right = NULL;
            node->left = temp;
            setMessage(temp->dic.key, 'a');
            repaint();
        }
        else
            addItemRec(node->left, d);

    } else if (strcmp(node->dic.key, d.key) < 0) {
        if (node->right == NULL) {
            struct item *temp = (struct item *) calloc(1, sizeof(struct item));
            strcpy(temp->dic.key, d.key);
            strcpy(temp->dic.val, d.val);
            temp->left = NULL;
            temp->right = NULL;
            node->right = temp;
            setMessage(temp->dic.key, 'a');
            repaint();
        }
        else
            addItemRec(node->right, d);
    }
}

void deleteAll(){
    deleteRec(descr.root);
    descr.root = NULL;
    setMessage("Дерево очищено", 's');
    repaint();
}

void deleteRec(struct item* item){
    if (item == NULL)
        return;
    if (item->right != NULL)
        deleteRec(item->right);
    if (item->left != NULL)
        deleteRec(item->left);
    free(item);
}

void onFindPathClick(){
    setMessage("Выберите первый элемент", 's');
    findPathState.isActive = 1;
}

void onDetourStopClick(GtkButton* button){
    gtk_widget_destroy(GTK_WIDGET(detourDialog.detourdialog));
    repaint();
    setMessage("Обход завершен", 's');
}

void recDetour(struct item* item, int a){
    draw_one_element(item, a);
    if (item->right != NULL)
        recDetour(item->right, (a << 1) + 1);
    if (item->left != NULL)
        recDetour(item->left, a << 1);
}

void onDetourNextClick(GtkButton* button){
    if (detourDialog.type==1){
        if (detourDialog.temp != NULL) {
            recDetour(detourDialog.temp, 1);
            detourDialog.temp = NULL;
        }
        else
            onDetourStopClick(button);
    }
    else if (detourDialog.type==2){
        if (!stackIsEmpty()){
            struct Stack* s = stackGetOne();
            detourDialog.temp = s->item;
            draw_one_element(detourDialog.temp, s->a);
            if (detourDialog.temp->right != NULL)
                stackPutOne(detourDialog.temp->right, ((s->a) << 1) + 1);
            if (detourDialog.temp->left != NULL)
                stackPutOne(detourDialog.temp->left, (s->a) << 1);
        }
        else
            onDetourStopClick(button);
    }
    else if (detourDialog.type==3){
        if (!queueIsEmpty()){
            struct Queue* q = queueGetOne();
            detourDialog.temp = q->item;
            draw_one_element(detourDialog.temp, q->a);
            if (detourDialog.temp->left != NULL)
                queuePutOne(detourDialog.temp->left, (q->a) << 1);
            if (detourDialog.temp->right != NULL)
                queuePutOne(detourDialog.temp->right, ((q->a) << 1) + 1);
        }
        else
            onDetourStopClick(button);
    }
}

void onDetourClick(){
    setMessage("Обход", 's');

    GtkBuilder* builder = gtk_builder_new ();
    GError* error = NULL;
    if (!gtk_builder_add_from_file (builder, "../resources/layout/detourDialog.glade", &error)) {
        g_critical ("Не могу загрузить файл: %s", error->message);
        g_error_free (error);
    }
    gtk_builder_connect_signals (builder, NULL);
    detourDialog.detourdialog = GTK_BOX(gtk_builder_get_object(builder, "detourdialog"));
    gtk_widget_set_css_provider(GTK_WIDGET(detourDialog.detourdialog), treeView.css);
    GtkButton* butStop = GTK_BUTTON(gtk_builder_get_object(builder, "butStop"));
    GtkButton* butNext = GTK_BUTTON(gtk_builder_get_object(builder, "butNext"));
    g_signal_connect_swapped(G_OBJECT(butStop), "clicked", G_CALLBACK(onDetourStopClick), butStop);
    g_signal_connect_swapped(G_OBJECT(butNext), "clicked", G_CALLBACK(onDetourNextClick), butNext);

    if (gtk_check_menu_item_get_active(GTK_CHECK_MENU_ITEM(detourDialog.miR))) {
        detourDialog.type = 1;
        gtk_container_clear(GTK_CONTAINER(treeView.field));
        detourDialog.temp = descr.root;
    }
    if (gtk_check_menu_item_get_active(GTK_CHECK_MENU_ITEM(detourDialog.miS))) {
        detourDialog.type = 2;
        gtk_container_clear(GTK_CONTAINER(treeView.field));
        createStack();
        detourDialog.temp = descr.root;
        stackPutOne(detourDialog.temp, 1);
    }
    if (gtk_check_menu_item_get_active(GTK_CHECK_MENU_ITEM(detourDialog.miQ))) {
        detourDialog.type = 3;
        gtk_container_clear(GTK_CONTAINER(treeView.field));
        createQueue();
        detourDialog.temp = descr.root;
        queuePutOne(detourDialog.temp, 1);
    }
    gtk_box_pack_start(treeView.statusbar, GTK_WIDGET(detourDialog.detourdialog), gtk_false(), gtk_false(), 0);
}

struct item* findCommonParent(struct item* first, struct item* second){
    createQueue();
    struct item* temp = descr.root;
    queuePut(temp);
    while (!queueIsEmpty()) {
        temp = queueGet();
        printf("temp: %s\n", temp->dic.key);
        if ((strcmp(first->dic.key, temp->dic.key) > 0 && strcmp(second->dic.key, temp->dic.key) < 0)
                || (strcmp(first->dic.key, temp->dic.key) < 0 && strcmp(second->dic.key, temp->dic.key) > 0)){
            return temp;
        }
        if (temp->left != NULL)
            queuePut(temp->left);
        if (temp->right != NULL)
            queuePut(temp->right);
    }
    return NULL;
}

int calcDistance(struct item* first, struct item* second){
    int dist = 0;
    struct item* temp = first;
    while (temp != second){
        if (strcmp(temp->dic.key, second->dic.key) > 0) {
            if (temp->left==NULL)
                return -1;
            temp = temp->left;
            dist++;
        }
        else if (strcmp(temp->dic.key, second->dic.key) < 0) {
            if (temp->right==NULL)
                return -1;
            temp = temp->right;
            dist++;
        }
    }
    return dist;
}

void findPath(const char* first, const char* second){
    struct item* fItem = findByKey(descr.root, first);
    struct item* sItem = findByKey(descr.root, second);
    if (fItem==sItem) {
        char s[5];
        itoa(0, s);
        setMessage(s, 'r');
        return;
    }
    int distFS = calcDistance(fItem, sItem);
    int distSF = calcDistance(sItem, fItem);
    if (distFS > 0 || distSF > 0){
        if (distFS > 0) {
            char s[5];
            itoa(distFS, s);
            setMessage(s, 'r');
            return;
        }
        else if (distSF > 0) {
            char s[5];
            itoa(distSF, s);
            setMessage(s, 'r');
            return;
        }
    }
    else{
        struct item* parent = findCommonParent(fItem, sItem);
        int dist = calcDistance(parent, fItem) + calcDistance(parent, sItem);
        char s[5];
        itoa(dist, s);
        setMessage(s, 'r');
    }
}

void onEditBreakClick(struct item* item){
    gtk_entry_set_text(item->view.entVal, item->dic.val);
    gtk_editable_set_editable(GTK_EDITABLE(item->view.entVal), gtk_false());
    gtk_widget_destroy(GTK_WIDGET(editDialog.editdialog));
    setMessage("Редактирование отменено", 's');
}

void onEditConfClick(struct item* item){
    strcpy(item->dic.val, gtk_entry_get_text(item->view.entVal));
    gtk_editable_set_editable(GTK_EDITABLE(item->view.entVal), gtk_false());
    gtk_widget_destroy(GTK_WIDGET(editDialog.editdialog));
    setMessage(item->dic.key, 'c');
}

void onEditItemClick(const char* s){
    setMessage("Редактирование", 's');
    struct item* item1 = findByKey(descr.root, s);

    GtkBuilder* builder = gtk_builder_new ();
    GError* error = NULL;
    if (!gtk_builder_add_from_file (builder, "../resources/layout/leaf.glade", &error)) {
        g_critical ("Не могу загрузить файл: %s", error->message);
        g_error_free (error);
    }
    gtk_builder_connect_signals (builder, NULL);
    editDialog.editdialog = GTK_BOX(gtk_builder_get_object(builder, "editdialog"));
    gtk_widget_set_css_provider(GTK_WIDGET(editDialog.editdialog), treeView.css);
    GtkButton* butBreak = GTK_BUTTON(gtk_builder_get_object(builder, "butBreak"));
    GtkButton* butConf = GTK_BUTTON(gtk_builder_get_object(builder, "butConf"));
    g_signal_connect_swapped(G_OBJECT(butBreak), "clicked", G_CALLBACK(onEditBreakClick), item1);
    g_signal_connect_swapped(G_OBJECT(butConf), "clicked", G_CALLBACK(onEditConfClick), item1);

    gtk_box_pack_start(treeView.statusbar, GTK_WIDGET(editDialog.editdialog), gtk_false(), gtk_false(), 0);
    gtk_editable_set_editable(GTK_EDITABLE(item1->view.entVal), gtk_true());
}

void onDeleteItemClick(const char* s){
    struct item* item1 = findByKey(descr.root, s);
    setMessage(item1->dic.key, 'd');
    struct item* parent = findParentByKey(descr.root, s);

    //случай удаления элемента без ветвей
    if (item1->left == NULL && item1->right == NULL){
        if (parent == NULL) {
            deleteAll();
            return;
        }
        else if (parent->right == item1)
            parent->right = NULL;
        else if (parent->left == item1)
            parent->left = NULL;
        free(item1);
        repaint();
    }

    //случай удаления элемента с одной ветвью
    else if (item1->left != NULL ^ item1->right != NULL){
        if (parent == NULL){
            if (item1->left != NULL)
                descr.root = item1->left;
            else if (item1->right != NULL)
                descr.root = item1->right;
        }
        else if (parent->right == item1) {
            if (item1->left != NULL)
                parent->right = item1->left;
            else if (item1->right != NULL)
                parent->right = item1->right;
        }
        else if (parent->left == item1) {
            if (item1->left != NULL)
                parent->left = item1->left;
            else if (item1->right != NULL)
                parent->left = item1->right;
        }
        free(item1);
        repaint();
    }

    //случай удаления элемента с двумя ветвями
    else if (item1->left != NULL && item1->right != NULL) {
        struct item *temp = item1->right;
        while (temp->left)
            temp = temp->left;
        struct item *tempPar = findParentByKey(descr.root, temp->dic.key);

        if (parent == NULL){
            descr.root = temp;
        }
        else if (parent->right == item1)
            parent->right = temp;
        else if (parent->left == item1)
            parent->left = temp;

        if (temp != item1->right)
            temp->right = item1->right;
        temp->left = item1->left;
        tempPar->left = NULL;

        free(item1);
        repaint();
    }
}

void drawItem(struct item* item, int x, int y){
    GtkBuilder* builder = gtk_builder_new ();
    GError* error = NULL;
    if (!gtk_builder_add_from_file (builder, "../resources/layout/leaf.glade", &error)) {
        g_critical ("Не могу загрузить файл: %s", error->message);
        g_error_free (error);
    }
    gtk_builder_connect_signals (builder, NULL);

    item->view.cell = GTK_BOX(gtk_builder_get_object(builder, "leaf"));
    gtk_widget_set_css_provider(GTK_WIDGET(item->view.cell), treeView.css);

    item->view.menu = GTK_MENU(gtk_builder_get_object(builder, "contextmenu"));
    GtkMenuItem* editItem = GTK_MENU_ITEM(gtk_builder_get_object(builder, "editMenuItem"));
    GtkMenuItem* deleteItem = GTK_MENU_ITEM(gtk_builder_get_object(builder, "deleteMenuItem"));
    g_signal_connect_swapped(G_OBJECT(editItem), "activate", G_CALLBACK(onEditItemClick), item->dic.key);
    g_signal_connect_swapped(G_OBJECT(deleteItem), "activate", G_CALLBACK(onDeleteItemClick), item->dic.key);
    g_signal_connect_swapped(G_OBJECT(item->view.cell), "button-press-event", G_CALLBACK(show_popup), item);

    item->view.entKey = GTK_ENTRY(gtk_builder_get_object(builder, "entryKey"));
    item->view.entVal = GTK_ENTRY(gtk_builder_get_object(builder, "entryVal"));
    gtk_entry_set_text(item->view.entKey, item->dic.key);
    gtk_editable_set_editable(GTK_EDITABLE(item->view.entKey), gtk_false());
    gtk_entry_set_text(item->view.entVal, item->dic.val);
    gtk_editable_set_editable(GTK_EDITABLE(item->view.entVal), gtk_false());

    gtk_fixed_put(treeView.field, GTK_WIDGET(item->view.cell), x, y);
}

struct item* findParentByKey(struct item* item, const char* k){
    createQueue();
    struct item* temp = item;
    queuePut(temp);
    while (!queueIsEmpty()) {
        temp = queueGet();
        if (temp->left != NULL) {
            if (strcmp(temp->left->dic.key, k)==0)
                return temp;
            queuePut(temp->left);
        }
        if (temp->right != NULL) {
            if (strcmp(temp->right->dic.key, k)==0)
                return temp;
            queuePut(temp->right);
        }
    };
    return NULL;
}

struct item* findByKey(struct item* item, const char* k){
    createQueue();
    struct item* temp = item;
    queuePut(temp);
    while (!queueIsEmpty()) {
        temp = queueGet();
        if (strcmp(temp->dic.key, k)==0)
            return temp;
        if (temp->left != NULL)
            queuePut(temp->left);
        if (temp->right != NULL)
            queuePut(temp->right);
    }
    return NULL;
}

int getNumOfLevels(struct item* node, int n){
    static int num;
    if (n==0)
        num = 0;
    if (node == NULL)
        return 0;
    if (node->left != NULL)
        getNumOfLevels(node->left, n+1);
    if (node->right != NULL)
        getNumOfLevels(node->right, n+1);
    if (node->right == NULL && node->left == NULL)
        if (n > num)
            num = n;
    return num;
}

void setMessage(const char* s, char t){
    if (t=='s')
        gtk_label_set_text(treeView.mes, s);
    else if (t=='a'){
        char* str = (char*) calloc((9+20+10), sizeof(char));
        strcat(str, "Элемент \"");
        strcat(str, s);
        strcat(str, "\" добавлен");
        gtk_label_set_text(treeView.mes, str);
    }
    else if (t=='e'){
        char* str = (char*) calloc((9+20+16), sizeof(char));
        strcat(str, "Элемент \"");
        strcat(str, s);
        strcat(str, "\" уже существует");
        gtk_label_set_text(treeView.mes, str);
    }
    else if (t=='c'){
        char* str = (char*) calloc((9+20+9), sizeof(char));
        strcat(str, "Элемент \"");
        strcat(str, s);
        strcat(str, "\" изменен");
        gtk_label_set_text(treeView.mes, str);
    }
    else if (t=='d'){
        char* str = (char*) calloc((9+20+8), sizeof(char));
        strcat(str, "Элемент \"");
        strcat(str, s);
        strcat(str, "\" удален");
        gtk_label_set_text(treeView.mes, str);
    }
    else if (t=='r'){
        char* str = (char*) calloc((12+5), sizeof(char));
        strcat(str, "Расстояние: ");
        strcat(str, s);
        gtk_label_set_text(treeView.mes, str);
    }
}