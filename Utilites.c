//
// Created by andrew on 25.05.18.
//

#include <string.h>
#include <stdlib.h>
#include "Utilites.h"

/*char* itoa(int a){
    char s[20];
    int i = 0;
    do {
        s[i++] = a % 10 + '0';
    } while ((a /= 10) > 0);
    s[i] = '\0';

    char c;
    int j;
    for (i = 0, j = strlen(s)-1; i<j; i++, j--) {
        c = s[i];
        s[i] = s[j];
        s[j] = c;
    }
    char* str = (char*) calloc(strlen(s), sizeof(char));
    for (int i=0; i<strlen(s); i++){
        str[i] = s[i];
    }
    return str;
}*/

void reverse(char s[])
{
    int i, j;
    char c;

    for (i = 0, j = strlen(s)-1; i<j; i++, j--) {
        c = s[i];
        s[i] = s[j];
        s[j] = c;
    }
}

void itoa(int n, char s[]) {
    int i, sign;

    if ((sign = n) < 0)  /* записываем знак */
        n = -n;          /* делаем n положительным числом */
    i = 0;
    do {       /* генерируем цифры в обратном порядке */
        s[i++] = n % 10 + '0';   /* берем следующую цифру */
    } while ((n /= 10) > 0);     /* удаляем */
    if (sign < 0)
        s[i++] = '-';
    s[i] = '\0';
    reverse(s);
}