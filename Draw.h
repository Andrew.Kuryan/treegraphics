//
// Created by andrew on 25.05.18.
//

#include "TreeView.h"

#ifndef TREEGRAPHICS_DRAW_H
#define TREEGRAPHICS_DRAW_H

struct FindPathState {
    int isActive;
    char firstItem[20];
    char secondItem[20];
}findPathState;

void repaint();
void repaint_rec(struct item *item, int a);

void gtk_container_clear(GtkContainer *container);
void gtk_draw_line(GtkFixed *fixed, double x1, double y1, double x2, double y2);
void draw_one_element(struct item *item, int a);
int show_popup(struct item *item, GdkEvent* event);
void gtk_widget_set_css_provider(GtkWidget* widget, GtkCssProvider* provider);
void gtk_widget_bring_to_front(GtkWidget* widget);

int get_x_gap(int lev);
int get_right_gap(int lev);
int get_lev_by_a(int a);

#endif //TREEGRAPHICS_DRAW_H
