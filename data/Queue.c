//
// Created by andrew on 24.05.18.
//

#include <stdio.h>
#include <stdlib.h>
#include "Queue.h"

struct {
    struct Queue* head;
    struct Queue* tail;
}queueDescr;

void createQueue(){
    queueDescr.head = NULL;
    queueDescr.tail = NULL;
}

void queuePut(struct item *item){
    struct Queue* queue;
    if (!(queue = (struct Queue*) calloc(1, sizeof(struct Queue)))){
        printf("Недостаточно свободной памяти");
        return;
    }
    queue->item = item;

    if (queueDescr.tail != NULL)
        queueDescr.tail->next = queue;
    queue->next = NULL;
    queueDescr.tail = queue;

    if (queueDescr.head==NULL){
        queueDescr.head = queue;
    }
}

void queuePutOne(struct item *item, int a){
    struct Queue* queue;
    if (!(queue = (struct Queue*) calloc(1, sizeof(struct Queue)))){
        printf("Недостаточно свободной памяти");
        return;
    }
    queue->item = item;
    queue->a = a;

    if (queueDescr.tail != NULL)
        queueDescr.tail->next = queue;
    queue->next = NULL;
    queueDescr.tail = queue;

    if (queueDescr.head==NULL){
        queueDescr.head = queue;
    }
}

struct item* queueGet(){
    struct Queue* queue = queueDescr.head;
    queueDescr.head = queueDescr.head->next;
    return queue->item;
}

struct Queue* queueGetOne(){
    struct Queue* queue = queueDescr.head;
    queueDescr.head = queueDescr.head->next;
    return queue;
}

int queueIsEmpty(){
    if (queueDescr.head==NULL)
        return 1;
    else
        return 0;
}

void destroyQueue(){
    struct Queue * temp = queueDescr.head;
    while(queueDescr.head != queueDescr.tail)
    {
        queueDescr.head = queueDescr.head->next;
        free(temp);
        temp = queueDescr.head;
    }
    if(queueDescr.tail)
        free(queueDescr.tail);
}