//
// Created by andrew on 26.05.18.
//

#include "../TreeView.h"

#ifndef TREEGRAPHICS_STACK_H
#define TREEGRAPHICS_STACK_H

struct Stack{
    int a;
    struct item* item;
    struct Stack* next;
};

void createStack();
void stackPut(struct item *item);
void stackPutOne(struct item *item, int a);
struct item* stackGet();
struct Stack* stackGetOne();
int stackIsEmpty();
void destroyQueue();

#endif //TREEGRAPHICS_STACK_H
