//
// Created by andrew on 24.05.18.
//

#include "../TreeView.h"

#ifndef TREEGRAPHICS_QUEUE_H
#define TREEGRAPHICS_QUEUE_H

struct Queue{
    int a;
    struct item* item;
    struct Queue* next;
};

void createQueue();
void queuePut(struct item *item);
void queuePutOne(struct item *item, int a);
struct item* queueGet();
struct Queue* queueGetOne();
int queueIsEmpty();
void destroyQueue();


#endif //TREEGRAPHICS_QUEUE_H
