//
// Created by andrew on 26.05.18.
//

#include <stdlib.h>
#include <stdio.h>
#include "Stack.h"

struct {
    struct Stack* head;
}stackDescr;

void createStack(){
    stackDescr.head = NULL;
}

void stackPut(struct item* item){
    struct Stack* stack;
    if (!(stack = (struct Stack*) calloc(1, sizeof(struct Stack)))){
        printf("Недостаточно свободной памяти");
        return;
    }
    stack->item = item;

    stack->next = stackDescr.head;
    stackDescr.head = stack;
}

void stackPutOne(struct item* item, int a){
    struct Stack* stack;
    if (!(stack = (struct Stack*) calloc(1, sizeof(struct Stack)))){
        printf("Недостаточно свободной памяти");
        return;
    }
    stack->item = item;
    stack->a = a;

    stack->next = stackDescr.head;
    stackDescr.head = stack;
}

struct item* stackGet(){
    struct Stack* stack = stackDescr.head;
    stackDescr.head = stackDescr.head->next;
    return stack->item;
}

struct Stack* stackGetOne(){
    struct Stack* stack = stackDescr.head;
    stackDescr.head = stackDescr.head->next;
    return stack;
}

int stackIsEmpty(){
    if (stackDescr.head==NULL)
        return 1;
    else
        return 0;
}

void freeList(struct Stack* item){
    if(item){
        freeList(item->next);
        free(item);
    }
}

void destroyStack(){
    freeList(stackDescr.head);
}