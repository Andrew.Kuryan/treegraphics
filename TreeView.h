//
// Created by andrew on 22.05.18.
//

#include <gtk/gtk.h>

#ifndef TREEGRAPHICS_TREEVIEW_H
#define TREEGRAPHICS_TREEVIEW_H

struct{
    GtkCssProvider* css;
    GtkFixed* field;
    GtkImage* stub;
    GtkBox* statusbar;
    GtkLabel* mes;
    int xSize, ySize;                                        //1060 * 620
    int elWidth, elHeight;                                   //200 * 100
    int borderTop, borderRight, borderBottom, borderLeft;    //20   20    20    20
    int xGap, yGap;                                          //20   50
}treeView;

struct TreeObjects{
    GtkCssProvider* css;
    GtkFixed* field;
    GtkImage* stub;
    GtkBox* statusbar;
    GtkLabel* mes;
};

struct value{
    char key[20];
    char val[20];
};

struct view{
    GtkBox* cell;
    GtkEntry* entKey;
    GtkEntry* entVal;
    GtkMenu* menu;
};

struct item{
    struct value dic;

    struct item* left;
    struct item* right;

    struct view view;
};

struct {

    struct item* root;
}descr;

struct {
    int isActive;
    int type;

    int curSide;
    struct item* temp;

    GtkRadioMenuItem *miR, *miS, *miQ;
    GtkBox* detourdialog;
}detourDialog;

struct {
    int isActve;
    GtkBox* editdialog;
}editDialog;

void initTreeView(struct TreeObjects treeObjects);
void addItem(struct value d);
void addItemRec(struct item *node, struct value d);
void deleteAll();
void deleteRec(struct item* item);
void findPath(const char* first, const char* second);

void drawItem(struct item* item, int x, int y);
void setMessage(const char* s, char t);
struct item* findByKey(struct item* item, const char* k);
struct item* findParentByKey(struct item* item, const char* k);
int getNumOfLevels(struct item* node, int n);

#endif //TREEGRAPHICS_TREEVIEW_H
