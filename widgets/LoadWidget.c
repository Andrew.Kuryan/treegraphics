//
// Created by andrew on 26.05.18.
//

#include <gtk/gtk.h>
#include <string.h>
#include <stdlib.h>
#include "LoadWidget.h"
#include "../TreeView.h"
#include "../Draw.h"

void selectLoad(GtkWindow *parent) {
    GtkBuilder *builder;
    GError *error = NULL;
    builder = gtk_builder_new();
    if (!gtk_builder_add_from_file(builder, "../resources/layout/LoadFrame.glade", &error)) {
        g_critical ("Не могу загрузить файл: %s", error->message);
        g_error_free(error);
    }
    gtk_builder_connect_signals(builder, NULL);
    loadWidget.widget = GTK_FILE_CHOOSER_DIALOG(gtk_builder_get_object(builder, "loadwindow"));
    gtk_window_set_transient_for(loadWidget.widget, parent);
    gtk_widget_show(loadWidget.widget);
}

void cancLoad(GtkButton* button){
    gtk_window_close(loadWidget.widget);
}

struct item* readFromFile(FILE *f){
    struct item* temp = (struct item*) calloc(1, sizeof(struct item));
    fscanf(f, "%s", temp->dic.key);
    fscanf(f, "%s", temp->dic.val);
    if (strcmp(temp->dic.key, "-")==0 && strcmp(temp->dic.val, "-")==0){
        return NULL;
    }
    else{
        temp->left = readFromFile(f);
        temp->right = readFromFile(f);
        return temp;
    }
}

void loadConf(GtkButton* button) {
    const char *str = gtk_file_chooser_get_filename(loadWidget.widget);
    FILE *f = fopen(str, "r");

    deleteAll();
    gtk_container_remove(GTK_CONTAINER(treeView.field), GTK_WIDGET(treeView.stub));
    gtk_widget_set_css_provider(GTK_WIDGET(treeView.field), treeView.css);
    descr.root = readFromFile(f);
    repaint();
    setMessage("Загружено из файла", 's');

    fclose(f);
    gtk_window_close(loadWidget.widget);
}