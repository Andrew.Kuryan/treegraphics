//
// Created by andrew on 26.05.18.
//

#include <gtk/gtk.h>
#include "../TreeView.h"
#include "SaveWidget.h"

void selectSave(GtkWindow *parent) {
    GtkBuilder *builder;
    GError *error = NULL;
    builder = gtk_builder_new();
    if (!gtk_builder_add_from_file(builder, "../resources/layout/SaveFrame.glade", &error)) {
        g_critical ("Не могу загрузить файл: %s", error->message);
        g_error_free(error);
    }
    gtk_builder_connect_signals(builder, NULL);
    saveWidget.widget = GTK_FILE_CHOOSER_DIALOG(gtk_builder_get_object(builder, "savewindow"));
    gtk_window_set_transient_for(saveWidget.widget, parent);
    gtk_widget_show(saveWidget.widget);
}

void cancSave(GtkButton* button){
    gtk_window_close(saveWidget.widget);
}

void saveInFile(FILE *f, struct item* item){
    if (item==NULL) {
        fprintf(f, "%s %s\n", "-", "-");
        return;
    }
    fprintf(f, "%s %s\n", item->dic.key, item->dic.val);
    saveInFile(f, item->left);
    saveInFile(f, item->right);
}

void saveConf(GtkButton* button){
    const char* str = gtk_file_chooser_get_filename(saveWidget.widget);
    FILE *f = fopen(str, "w");

    saveInFile(f, descr.root);
    setMessage("Сохранено в файл", 's');

    fclose(f);
    gtk_window_close(saveWidget.widget);
}