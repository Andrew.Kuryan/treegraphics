#include <stdio.h>
#include <string.h>
#include <gtk/gtk.h>
#include "TreeView.h"
#include "data/Queue.h"
#include "Draw.h"
#include "widgets/SaveWidget.h"
#include "widgets/LoadWidget.h"

GtkWindow* window;
GtkCssProvider* css;
GtkFixed* center;
GtkBox* extra;
GtkImage* null;
GtkBox* statusbar;
GtkLabel* mess;

void onDeleteAllClick();

struct {
    GtkBuilder* builder;
    GtkDialog* dialog;
    GtkEntry* entKey;
    GtkEntry* entVal;
}addDialog;

struct {
    GtkBuilder* builder;
    GtkDialog* dialog;
}deleteDialog;



void onSaveClick(){
    selectSave(window);
}

void onLoadClick(){
    selectLoad(window);
}

void onExitClick(){
    onDeleteAllClick();
    gtk_window_close(window);
}

void onAddClick(){
    addDialog.builder = gtk_builder_new ();
    GError* error = NULL;
    if (!gtk_builder_add_from_file (addDialog.builder, "../resources/layout/adddialog.glade", &error)) {
        g_critical ("Не могу загрузить файл: %s", error->message);
        g_error_free (error);
    }
    addDialog.dialog = GTK_DIALOG(gtk_builder_get_object(addDialog.builder, "adddialog"));
    gtk_builder_connect_signals (addDialog.builder, NULL);
    gtk_window_set_transient_for(GTK_WINDOW(addDialog.dialog), window);
    gtk_widget_show(GTK_WIDGET(addDialog.dialog));
}

void onAddConfClick(){
    addDialog.entKey = GTK_ENTRY(gtk_builder_get_object(addDialog.builder, "entryKey"));
    addDialog.entVal = GTK_ENTRY(gtk_builder_get_object(addDialog.builder, "entryVal"));
    struct value v;
    strcpy(v.key, gtk_entry_get_text(addDialog.entKey));
    strcpy(v.val, gtk_entry_get_text(addDialog.entVal));
    addItem(v);
    gtk_window_close(GTK_WINDOW(addDialog.dialog));
}

void onDeleteAllClick(){
    if (descr.root != NULL) {
        deleteDialog.builder = gtk_builder_new();
        GError *error = NULL;
        if (!gtk_builder_add_from_file(deleteDialog.builder, "../resources/layout/deletedialog.glade", &error)) {
            g_critical ("Не могу загрузить файл: %s", error->message);
            g_error_free(error);
        }
        deleteDialog.dialog = GTK_DIALOG(gtk_builder_get_object(deleteDialog.builder, "deletedialog"));
        gtk_builder_connect_signals(deleteDialog.builder, NULL);
        gtk_window_set_transient_for(GTK_WINDOW(deleteDialog.dialog), window);
        gtk_widget_show(GTK_WIDGET(deleteDialog.dialog));
    }
}

void onDeleteAllConfClick(){
    deleteAll();
    gtk_window_close(GTK_WINDOW(deleteDialog.dialog));
}

static GtkWidget* create_window () {

    GtkWidget *window;
    GtkBuilder *builder;
    GError* error = NULL;

    builder = gtk_builder_new ();
    if (!gtk_builder_add_from_file (builder, "../resources/layout/lr7new.glade", &error)) {
        g_critical ("Не могу загрузить файл: %s", error->message);
        g_error_free (error);
    }
    gtk_builder_connect_signals (builder, NULL);

    css = gtk_css_provider_new();
    gtk_css_provider_load_from_path(css, "../resources/styles/design.css", NULL);

    window = GTK_WIDGET (gtk_builder_get_object (builder, "window"));
    if (!window)
        g_critical ("Ошибка при получении виджета окна");

    detourDialog.miR = GTK_RADIO_MENU_ITEM(gtk_builder_get_object(builder, "miRecEnt"));
    detourDialog.miS = GTK_RADIO_MENU_ITEM(gtk_builder_get_object(builder, "miStEnt"));
    detourDialog.miQ = GTK_RADIO_MENU_ITEM(gtk_builder_get_object(builder, "miQEnt"));

    GtkSeparator* sep1 = GTK_SEPARATOR(gtk_builder_get_object(builder, "separator1"));
    gtk_widget_set_css_provider(GTK_WIDGET(sep1), css);
    GtkSeparator* sep2 = GTK_SEPARATOR(gtk_builder_get_object(builder, "separator2"));
    gtk_widget_set_css_provider(GTK_WIDGET(sep2), css);
    GtkBox* tools = GTK_BOX(gtk_builder_get_object(builder, "instruments"));
    gtk_widget_set_css_provider(GTK_WIDGET(tools), css);
    GtkBox* status = GTK_BOX(gtk_builder_get_object(builder, "statusbar"));
    gtk_widget_set_css_provider(GTK_WIDGET(status), css);

    null = GTK_IMAGE(gtk_builder_get_object(builder, "null"));
    center = GTK_FIXED(gtk_builder_get_object(builder, "boxCenter"));
    extra = GTK_BOX(gtk_builder_get_object(builder, "extraPanel"));
    statusbar = GTK_BOX(gtk_builder_get_object(builder, "statusbar"));
    mess = GTK_LABEL(gtk_builder_get_object(builder, "labInfo"));
    gtk_label_set_text(mess, "Приложение запущено");
    struct TreeObjects treeObjects;
    treeObjects.field = center;
    treeObjects.statusbar = statusbar;
    treeObjects.stub = null;
    treeObjects.mes = mess;
    treeObjects.css = css;

    initTreeView(treeObjects);

    g_object_unref (G_OBJECT(builder));

    return window;
}

int main(int argc, char *argv[]) {
    gtk_init (&argc, &argv);

    window = GTK_WINDOW(create_window());
    gtk_window_set_title(window, "ЛР №7 (Деревья)");
    g_signal_connect(window, "destroy", G_CALLBACK(gtk_main_quit), NULL);
    gtk_widget_show (GTK_WIDGET(window));

    gtk_main ();
    return 0;
}