//
// Created by andrew on 25.05.18.
//

#include <gtk/gtk.h>
#include <math.h>
#include <string.h>
#include "Draw.h"

void gtk_container_clear(GtkContainer *container){
    GList *children, *iter;
    children = gtk_container_get_children(GTK_CONTAINER(container));
    for(iter = children; iter != NULL; iter = g_list_next(iter))
        gtk_widget_destroy(GTK_WIDGET(iter->data));
    g_list_free(children);
}

void gtk_draw_line(GtkFixed *fixed, double x1, double y1, double x2, double y2){
    const double pWidth = 10;
    const double step = 2;

    double yStep = step * (y2 - y1) / fabs(x2 - x1);
    double xStep = x2>x1 ? step : -step;
    for (x1; x1 != x2; x1+=xStep){
        GtkImage* point = GTK_IMAGE(gtk_image_new_from_file("../resources/images/point.png"));
        gtk_widget_set_visible(point, gtk_true());
        gtk_fixed_put(fixed, point, x1-pWidth/2, y1-pWidth/2);
        y1 += yStep;
    }
}

int show_popup(struct item *item, GdkEvent* event){
    const gint RIGHT_CLICK = 3;
    const gint LEFT_CLICK = 1;
    if (event->type == GDK_BUTTON_PRESS){
        GdkEventButton* bevent = (GdkEventButton*) event;
        if (bevent->button == RIGHT_CLICK) {
            gtk_menu_popup(GTK_MENU(item->view.menu), NULL, NULL, NULL, NULL, bevent->button, bevent->time);
        }
        if (bevent->button == LEFT_CLICK){
            if(findPathState.isActive==1) {
                strcpy(findPathState.firstItem, item->dic.key);
                findPathState.isActive = 2;
                setMessage("Выберите второй элемент", 's');
            }
            else if(findPathState.isActive==2){
                strcpy(findPathState.secondItem, item->dic.key);
                findPathState.isActive = 0;
                findPath(findPathState.firstItem, findPathState.secondItem);
            }
        }
        return TRUE;
    }
    return FALSE;
}

void gtk_widget_set_css_provider(GtkWidget* widget, GtkCssProvider* provider){
    gtk_style_context_add_provider(gtk_widget_get_style_context(GTK_WIDGET(widget)),
                                   GTK_STYLE_PROVIDER(provider), GTK_STYLE_PROVIDER_PRIORITY_USER);
}

void gtk_widget_bring_to_front(GtkWidget* widget){
    gint wx, wy;
    gtk_widget_translate_coordinates(widget, gtk_widget_get_toplevel(widget), 0, 0, &wx, &wy);
    printf("%s\n", gtk_widget_get_name(gtk_widget_get_toplevel(widget)));
}

void repaint() {
    gtk_container_clear(GTK_CONTAINER(treeView.field));
    if (descr.root == NULL) {
        gtk_widget_set_size_request(GTK_WIDGET(treeView.field), treeView.xSize, treeView.ySize);
        treeView.stub = GTK_IMAGE(gtk_image_new_from_file("../resources/images/null.png"));
        gtk_widget_set_visible(GTK_WIDGET(treeView.stub), gtk_true());
        gtk_fixed_put(treeView.field, GTK_WIDGET(treeView.stub), 0, 0);
    }
    else {
        gtk_widget_set_size_request(GTK_WIDGET(treeView.field),
                                    treeView.elWidth * (int) pow(2, getNumOfLevels(descr.root, 0))
                                    + treeView.xGap * (int) (pow(2, getNumOfLevels(descr.root, 0)) - 1) +
                                    treeView.borderRight + treeView.borderLeft,
                                    treeView.elHeight * (getNumOfLevels(descr.root, 0) + 1) +
                                    treeView.yGap * (getNumOfLevels(descr.root, 0)) +
                                    treeView.borderTop + treeView.borderBottom);
        repaint_rec(descr.root, 1);
    }
}

void repaint_rec(struct item *item, int a){
    draw_one_element(item, a);
    if (item->left != NULL)
        repaint_rec(item->left, a << 1);
    if (item->right != NULL)
        repaint_rec(item->right, (a << 1) + 1);
}

void draw_one_element(struct item *item, int a){
    const int lev = get_lev_by_a(a)-1;
    if (getNumOfLevels(descr.root, 0) != lev){
        if (item->left != NULL) {
            gtk_draw_line(treeView.field,
                          get_right_gap(lev) + (treeView.elWidth + get_x_gap(lev)) * (int) (a - pow(2, lev)) +
                          treeView.elWidth / 2,
                          treeView.borderTop + (treeView.elHeight + treeView.yGap) * lev + treeView.elHeight / 2,
                          get_right_gap(lev + 1) +
                          (treeView.elWidth + get_x_gap(lev + 1)) * (int) ((a << 1) - pow(2, lev + 1)) +
                          treeView.elWidth / 2,
                          treeView.borderTop + (treeView.elHeight + treeView.yGap) * (lev + 1) + treeView.elHeight / 2);
        }
        if (item->right != NULL) {
            gtk_draw_line(treeView.field,
                          get_right_gap(lev) + (treeView.elWidth + get_x_gap(lev)) * (int) (a - pow(2, lev)) +
                          treeView.elWidth / 2,
                          treeView.borderTop + (treeView.elHeight + treeView.yGap) * lev + treeView.elHeight / 2,
                          get_right_gap(lev + 1) +
                          (treeView.elWidth + get_x_gap(lev + 1)) * (int) (((a << 1) + 1) - pow(2, lev + 1)) +
                          treeView.elWidth / 2,
                          treeView.borderTop + (treeView.elHeight + treeView.yGap) * (lev + 1) + treeView.elHeight / 2);
        }
        drawItem(item, get_right_gap(lev) + (treeView.elWidth + get_x_gap(lev)) * (int) (a - pow(2, lev)),
                 treeView.borderTop + (treeView.elHeight + treeView.yGap) * lev);
    }
    else
        drawItem(item, treeView.borderRight + (treeView.elWidth + treeView.xGap) * (int) (a - pow(2, lev)),
                 treeView.borderTop + (treeView.elHeight + treeView.yGap) * lev);
}

int get_lev_by_a(int a){
    int lev = 0;
    while (a != 0){
        a = a/2;
        lev++;
    }
    return lev;
}

int get_x_gap(int lev){
    int invLev = getNumOfLevels(descr.root, 0)-lev;
    if (invLev==0)
        return treeView.xGap;
    else if (invLev==1)
        return treeView.xGap + 2 * (treeView.elWidth/2 + treeView.xGap/2);
    else
        return get_x_gap(lev + 1) + 2 * (treeView.elWidth + get_x_gap(lev + 2));
}

int get_right_gap(int lev){
    int invLev = getNumOfLevels(descr.root, 0)-lev;
    if (invLev==0)
        return treeView.borderRight;
    else if (invLev==1)
        return treeView.borderRight + treeView.elWidth / 2 + treeView.xGap / 2;
    else
        return get_right_gap(lev + 1) + treeView.elWidth + get_x_gap(lev + 2);
}